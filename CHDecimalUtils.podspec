Pod::Spec.new do |s|
  s.name             = 'CHDecimalUtils'
  s.version          = '0.1.0'
  s.summary          = '数值操作类'
  s.description      = <<-DESC
1. NSString及NSNumber类型数值的加减乘除计算;
2. 取值方式:向上,向下,四舍五入取整及保留两位小数
                       DESC

  s.homepage         = 'https://gitlab.com/EvilWizard/CHDecimalUtils.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'EvilWizard' => 'duanchao19900812@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/EvilWizard/CHDecimalUtils.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.source_files = 'CHDecimalUtils/Classes/**/*'
end
