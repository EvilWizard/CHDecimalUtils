//
//  DecimalUtils.h
//  MenDianBao
//
//  Created by 行者栖处 on 2018/4/4.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CHDecimalUtils : NSObject

#pragma mark - 加减乘除
/**
 *  加
 */
+ (NSDecimalNumber *)decimal1:(id)decimal1 addingByDecimal2:(id)decimal2;

/**
 *  减
 */
+ (NSDecimalNumber *)decimal1:(id)decimal1 subtractingByDecimal2:(id)decimal2;

/**
 *  乘
 */
+ (NSDecimalNumber *)decimal1:(id)decimal1 multiplyingByDecimal2:(id)decimal2;

/**
 *  除
 */
+ (NSDecimalNumber *)decimal1:(id)decimal1 dividingByDecimal2:(id)decimal2;

#pragma mark - 取值方式
/**
 向上取
 
 @param decimal number类型或者string类型值
 @param scale 取整小数点后位数
 @return 结果
 */
+ (NSDecimalNumber *)roundUpWith:(id)decimal scale:(short)scale;

/**
 向下取
 
 @param decimal number类型或者string类型值
 @param scale 取整小数点后位数
 @return 结果
 */
+ (NSDecimalNumber *)roundDownWith:(NSDecimalNumber *)decimal scale:(short)scale;

/**
 四舍五入取
 
 @param decimal number类型或者string类型值
 @param scale 取整小数点后位数
 @return 结果
 */
+ (NSDecimalNumber *)roundPlainWith:(NSDecimalNumber *)decimal scale:(short)scale;

@end
