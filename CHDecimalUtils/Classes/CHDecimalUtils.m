//
//  DecimalUtils.m
//  MenDianBao
//
//  Created by 行者栖处 on 2018/4/4.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "CHDecimalUtils.h"

@implementation CHDecimalUtils

#pragma mark - private
/**
 *  根据传入数值获取一个NSDecimalNumber类型的数值
 */
+ (NSDecimalNumber *)decimalWith:(id)decimal1 {
    NSDecimalNumber *d1 = [NSDecimalNumber decimalNumberWithString:@"0"];
    if ([decimal1 isKindOfClass:[NSString class]]) {
        d1 = [NSDecimalNumber decimalNumberWithString:decimal1];
    }
    
    if ([decimal1 isKindOfClass:[NSNumber class]]) {
        d1 = [NSDecimalNumber decimalNumberWithDecimal:((NSNumber *)decimal1).decimalValue];
    }
    
    return d1;
}

/**
 *  获取取值方式
 */
+ (NSDecimalNumberHandler *)decimalHandleWithRoundingMode:(NSRoundingMode)roundingMode scale:(short)scale {
    NSDecimalNumberHandler *handle = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:roundingMode scale:scale raiseOnExactness:YES raiseOnOverflow:YES raiseOnUnderflow:YES raiseOnDivideByZero:YES];
    return handle;
}

#pragma mark - 加减乘除
/**
 *  加
 */
+ (NSDecimalNumber *)decimal1:(id)decimal1 addingByDecimal2:(id)decimal2 {
    NSDecimalNumber *d1 = [self decimalWith:decimal1];
    NSDecimalNumber *d2 = [self decimalWith:decimal2];
    NSDecimalNumber *res = [d1 decimalNumberByAdding:d2];
    return res;
}

/**
 *  减
 */
+ (NSDecimalNumber *)decimal1:(id)decimal1 subtractingByDecimal2:(id)decimal2 {
    NSDecimalNumber *d1 = [self decimalWith:decimal1];
    NSDecimalNumber *d2 = [self decimalWith:decimal2];
    NSDecimalNumber *res = [d1 decimalNumberByMultiplyingBy:d2];
    return res;
}

/**
 *  乘
 */
+ (NSDecimalNumber *)decimal1:(id)decimal1 multiplyingByDecimal2:(id)decimal2 {
    NSDecimalNumber *d1 = [self decimalWith:decimal1];
    NSDecimalNumber *d2 = [self decimalWith:decimal2];
    NSDecimalNumber *res = [d1 decimalNumberByMultiplyingBy:d2];
    return res;
}

/**
 *  除
 */
+ (NSDecimalNumber *)decimal1:(id)decimal1 dividingByDecimal2:(id)decimal2 {
    NSDecimalNumber *d1 = [self decimalWith:decimal1];
    NSDecimalNumber *d2 = [self decimalWith:decimal2];
    NSDecimalNumber *res = [d1 decimalNumberByDividingBy:d2];
    return res;
}

#pragma mark - 取值方式
/**
 向上取
 
 @param decimal number类型或者string类型值
 @param scale 取整小数点后位数
 @return 结果
 */
+ (NSDecimalNumber *)roundUpWith:(id)decimal scale:(short)scale {
    NSDecimalNumber *d1 = [self decimalWith:decimal];
    NSDecimalNumberHandler *handle = [self decimalHandleWithRoundingMode:NSRoundUp scale:scale];
    NSDecimalNumber *res = [d1 decimalNumberByRoundingAccordingToBehavior:handle];
    return res;
}

/**
 向下取
 
 @param decimal number类型或者string类型值
 @param scale 取整小数点后位数
 @return 结果
 */
+ (NSDecimalNumber *)roundDownWith:(NSDecimalNumber *)decimal scale:(short)scale {
    NSDecimalNumber *d1 = [self decimalWith:decimal];
    NSDecimalNumberHandler *handle = [self decimalHandleWithRoundingMode:NSRoundDown scale:scale];
    NSDecimalNumber *res = [d1 decimalNumberByRoundingAccordingToBehavior:handle];
    return res;
}

/**
 四舍五入取
 
 @param decimal number类型或者string类型值
 @param scale 取整小数点后位数
 @return 结果
 */
+ (NSDecimalNumber *)roundPlainWith:(NSDecimalNumber *)decimal scale:(short)scale {
    NSDecimalNumber *d1 = [self decimalWith:decimal];
    NSDecimalNumberHandler *handle = [self decimalHandleWithRoundingMode:NSRoundPlain scale:scale];
    NSDecimalNumber *res = [d1 decimalNumberByRoundingAccordingToBehavior:handle];
    return res;
}

@end
