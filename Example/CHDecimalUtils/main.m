//
//  main.m
//  CHDecimalUtils
//
//  Created by coderdc on 04/11/2018.
//  Copyright (c) 2018 coderdc. All rights reserved.
//

@import UIKit;
#import "CHAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CHAppDelegate class]));
    }
}
