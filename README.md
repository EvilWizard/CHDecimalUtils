# CHDecimalUtils

[![CI Status](http://img.shields.io/travis/coderdc/CHDecimalUtils.svg?style=flat)](https://travis-ci.org/coderdc/CHDecimalUtils)
[![Version](https://img.shields.io/cocoapods/v/CHDecimalUtils.svg?style=flat)](http://cocoapods.org/pods/CHDecimalUtils)
[![License](https://img.shields.io/cocoapods/l/CHDecimalUtils.svg?style=flat)](http://cocoapods.org/pods/CHDecimalUtils)
[![Platform](https://img.shields.io/cocoapods/p/CHDecimalUtils.svg?style=flat)](http://cocoapods.org/pods/CHDecimalUtils)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CHDecimalUtils is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CHDecimalUtils'
```

## Author

coderdc, duanchao19900812@gmail.com

## License

CHDecimalUtils is available under the MIT license. See the LICENSE file for more info.
